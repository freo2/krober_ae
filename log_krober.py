#!env python

import krober
import serial
import csv
import time


if __name__ == '__main__':

    #prompt for human input for filename
    filedesc = input('Run Name (no .txt)>> ')
    filename = '%s_%s.txt' % (time.strftime('%y%m%d_%H%M'),
                              filedesc)

    
    # WINDOWS
    s = serial.Serial('COM9',
                     38400,
                     stopbits=serial.STOPBITS_TWO,
                     timeout=2)
    # LINUX
    #    s = serial.Serial('/dev/ttyUSB0',
    #                  38400,
    #                  stopbits=serial.STOPBITS_TWO,
    #                  timeout=2)


    # MAC 
    # # send command to start
    # s = serial.Serial('/dev/tty.usbserial-00001014',
    #                   38400,
    #                   stopbits=serial.STOPBITS_TWO,
    #                   timeout=2)

    cmd = krober.create_cmdstr(b'a1')
    s.write(cmd)

    #create file and initilaise csv writer
    fout = open(filename, 'w',newline='')
    writer = csv.writer(fout)

    # parse the data coming in
    while(1):
        buff = s.read(1)

        if buff == b'#':
            # next byte is the packet length
            buff = s.read(1)

            packet_len = ord(buff) - ord('0') - 1

            cmd = s.read(1)

            packet = s.read(packet_len)

            # check packet length to ensure data intact
            if len(packet) == 33:
                data_dict = (krober.parse_datapacket(packet))   # create dict from incoming data
                data_dict_vals = list(data_dict.values())   # extract values only from data
                line = [time.time()]    # use system time as first line in csv
                for value in data_dict_vals:
                    line.append(value)
                writer.writerow(line)   # write csv line in file
                fout.flush()

            # the next two bytes are the checksum, for now let's toss them
            buff = s.read(2)
