#!env python

import struct

import serial
from PyCRC.CRCCCITT import CRCCCITT


def create_cmdstr(s0):
    '''Create a command to send to the AeroplusE

    >>> create_cmdstr(b'a0')
    b'#2a0\xe1\xf9'
    '''

    i = len(s0) + ord('0')

    s = b'#' + bytes([i, ]) + s0

    # calculate crc16
    crc = CRCCCITT()
    crc.starting_value = 0x5555 # Krober checksum has a unique starting value
    checksum = crc.calculate(s[1:])

    s += bytes([checksum >> 8, checksum & 0xFF])

    return s


def parse_datapacket(data):
    fmt = '>HBBHHHHHHHHHHHHHBH' # issue fixed 24/1/2020. Requires Big Endian decoding '>'

    data_struct = struct.unpack(fmt, data)

    fields = ['time',
              'phase',
              'valve_state',
              'reservoir_pressure',
              'VA',
              'Vin_min',
              '3V3',
              'compressor_temp',
              'gas_temp',
              'purity',
              'Vin_max',
              'alarms',
              'cycle_time_1',
              'cycle_time_2',
              'gas_constant',
              'us_points',
              'us_system_status',
              'volume_flow']

    data_dict = {}
    for i in range(len(data_struct)):
        data_dict[fields[i]] = data_struct[i]

    return data_dict


if __name__ == '__main__':
    data = b'9\x9a\x02\x16\x06\x98\x13\xaf \x0e\r!\x01O\n\xf0&\x13"{\x00\x00\x02\x94\x02\x92JI%\x1f\xff\x0e\x13'
    print(parse_datapacket(data))
    '''
    s = serial.Serial('/dev/ttyUSB0', 38400, stopbits=serial.STOPBITS_TWO, timeout=2)
    # s = serial.Serial('/dev/ttyUSB0', 38400, timeout=2)

    s0 = b'a1'

    cmd = create_cmdstr(s0)

    print([hex(i) for i in cmd])

    print(cmd)

    s.write(cmd)

    with open('fout.txt', 'wb') as fout:
        while(1):
            q = s.read()
            print(q)
            fout.write(q)
            fout.flush()
    '''
